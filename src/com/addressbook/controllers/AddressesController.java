package com.addressbook.controllers;

import com.addressbook.model.AddressErrors;
import com.addressbook.model.AddressRequest;
import com.addressbook.model.AddressResponse;
import com.addressbook.model.Addresses;
import com.addressbook.services.AddressService;
import com.addressbook.validators.AddressValidator;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Controller
public class AddressesController {


    @Resource(name = "addressService")
    private AddressService addressService;

    @Resource(name = "addressValidator")
    private AddressValidator addressValidator;


    @RequestMapping(value = "/addresses/fetch", method = RequestMethod.GET)
    public @ResponseBody
    AddressResponse fetch() {
        List<Addresses> addresses=addressService.findAll();
        AddressResponse response=new AddressResponse();
        response.setSuccess(true);
        response.setTotal(addresses.size());
        response.setData(addresses);
        return response;
    }

    @RequestMapping(value = "/addresses/update", method = RequestMethod.POST,consumes = {"application/json"})
    public @ResponseBody
    AddressResponse update(@RequestBody AddressRequest addr) {
        AddressResponse response=new AddressResponse();
        BindingResult result=new BeanPropertyBindingResult(addr.getData(), "addr");
        addressValidator.validate(addr.getData(),result);
        if (result.hasErrors()){
            List<FieldError> errors=result.getFieldErrors();
            response.setSuccess(false);
            response.setMessage("You have some errors!");
            List<AddressErrors> err=new ArrayList<>();
            AddressErrors ae=new AddressErrors();
            err.add(ae);
            for (FieldError e : errors) {
                if (e.getField().equals("name")){
                    ae.setName(e.getCode());
                }
                if (e.getField().equals("address")){
                    ae.setAddress(e.getCode());
                }
                if (e.getField().equals("phone")){
                    ae.setPhone(e.getCode());
                }

            }
            response.setData(err);
        }
        else {
            addressService.update(addr.getData());
            response.setSuccess(true);
            response.setTotal(1);
            List<Addresses> list=new ArrayList<>();
            list.add(addr.getData());
            response.setData(list);
        }


        return response;
    }

    @RequestMapping(value = "/addresses/add", method = RequestMethod.POST,consumes = {"application/json"})
    public @ResponseBody
    AddressResponse add(@RequestBody AddressRequest addr) {
        AddressResponse response=new AddressResponse();
        BindingResult result=new BeanPropertyBindingResult(addr.getData(), "addr");
        addressValidator.validate(addr.getData(),result);
        if (result.hasErrors()){
            List<FieldError> errors=result.getFieldErrors();
            response.setSuccess(false);
            response.setMessage("You have some errors!");
            List<AddressErrors> err=new ArrayList<>();
            AddressErrors ae=new AddressErrors();
            err.add(ae);
            for (FieldError e : errors) {
                if (e.getField().equals("name")){
                    ae.setName(e.getCode());
                }
                if (e.getField().equals("address")){
                    ae.setAddress(e.getCode());
                }
                if (e.getField().equals("phone")){
                    ae.setPhone(e.getCode());
                }
            }
            response.setData(err);
        }
        else {
            addressService.add(addr.getData());
            response.setSuccess(true);
            response.setTotal(1);
            List<Addresses> list=new ArrayList<>();
            list.add(addr.getData());
            response.setData(list);
        }


        return response;

        /*addressService.add(addr.getData());
        AddressResponse response=new AddressResponse();
        response.setSuccess(false);
        response.setTotal(1);

        List<AddressErrors> list=new ArrayList<>();
        AddressErrors error=new AddressErrors();
        error.setName("Name field is required!");
        error.setPhone("Phone field has not specified!");
        error.setAddress("Test");
        list.add(error);
        response.setData(list);
        return response;*/

    }

    @RequestMapping(value = "/addresses/delete", method = RequestMethod.POST,consumes = {"application/json"})
    public @ResponseBody
    AddressResponse delete(@RequestBody AddressRequest addr) {
        addressService.delete(addr.getData().getId());
        AddressResponse response=new AddressResponse();
        response.setSuccess(true);
        response.setTotal(1);
        List<Addresses> list=new ArrayList<>();
        list.add(addr.getData());
        response.setData(list);
        return response;
    }



}