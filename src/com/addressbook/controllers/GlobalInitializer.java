package com.addressbook.controllers;


import com.addressbook.validators.AddressValidator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;


@ControllerAdvice
public class GlobalInitializer {


    @InitBinder
    public void globalBinder(WebDataBinder binder) {
        //binder.addValidators(new AddressValidator());
    }

}