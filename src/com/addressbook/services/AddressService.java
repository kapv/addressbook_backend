package com.addressbook.services;

import com.addressbook.model.Addresses;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service("addressService")
@Transactional
public class AddressService {

    @Resource(name="sessionFactory")
    private SessionFactory sessionFactory;



    public Addresses get( Integer id ) {
        Session session = sessionFactory.getCurrentSession();
        Addresses addr = (Addresses) session.get(Addresses.class, id);
        return addr;
    }
    public void add(Addresses addr) {
        Session session = sessionFactory.getCurrentSession();
        session.save(addr);
    }

    public void delete(Integer id) {
        Session session = sessionFactory.getCurrentSession();
        Addresses addr = (Addresses) session.get(Addresses.class, id);
        session.delete(addr);

    }
    public void update(Addresses addr) {
        Session session = sessionFactory.getCurrentSession();
        session.update(addr);
    }

    public List<Addresses> findAll(){
        Session session = sessionFactory.getCurrentSession();
        Criteria crit=session.createCriteria(Addresses.class);
        crit.addOrder(Order.asc("name"));
        List<Addresses> list=(List<Addresses>)crit.list();
        return list;
    }
}