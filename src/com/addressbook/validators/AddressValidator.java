package com.addressbook.validators;

import com.addressbook.model.AddressRequest;
import com.addressbook.model.Addresses;
import com.mysql.jdbc.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;



public class AddressValidator implements Validator {


    @Override
    public boolean supports(Class<?> aClass) {
        return Addresses.class.equals(aClass);
    }


    @Override
    public void validate(Object o, Errors errors) {
        Addresses addr=(Addresses)o;


        if (StringUtils.isNullOrEmpty(addr.getName())){
            errors.rejectValue("name","Name field cannot be empty!");
        }

        if (StringUtils.isNullOrEmpty(addr.getPhone())){
            errors.rejectValue("phone","Phone field cannot be empty!");
        }

        if (StringUtils.isNullOrEmpty(addr.getAddress())){
            errors.rejectValue("address","Address field cannot be empty!");
        }

    }
}